/*
 *
 */

private int stepX;
private int stepY;

void setup() {
  size(600, 600);
  stepX = width / 2;
  stepY = height / 2;
}

void draw() {  
  drawPoint();
}

private void drawPoint() {
  int x = generateRandomStep();
  int y = generateRandomStep();
  stepX += stepCloserToMouseCursor(mouseX, stepX, x);
  stepY += stepCloserToMouseCursor(mouseY, stepY, y);

  point(stepX, stepY);
}

private int generateRandomStep() {
  return int(random(3))-1;
}

private int stepCloserToMouseCursor(int mousePosition, int stepPosition, int randomNumber) {
  int mouseFollowing = getMouseFollowInclinationIndex(); 
  if (mousePosition < stepPosition && randomNumber > 0) {
   return randomNumber - mouseFollowing;
  } else if (mousePosition > stepPosition && randomNumber < 0 ) {
   return randomNumber + mouseFollowing;
  }
  return randomNumber;
}

private int getMouseFollowInclinationIndex() {
  // 25 % of possibility to walk towards mouse coursor
  return int(random(2)) * int(random(2));
}